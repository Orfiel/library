package orfiel.model;

public class Shelf {

    private Book[] books;


    public Shelf(int size) {
        books = new Book[size];
    }

    public void set(int index, Book book) {
        books[index] = book;

    }

    public Book get(int index) {
        return books[index];
    }

    public int getSize() {
        return books.length;
    }

    public void print() {
        for (Book book : books) {
            if (book != null) {
                book.print();
            }
        }
    }
}
