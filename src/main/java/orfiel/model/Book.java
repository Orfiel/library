package orfiel.model;

public class Book {

    private String author;
    private String title;
    private int pubYear;
    //zadeklarowanie zmiennych prywatnych

    public Book() {}      //konstruktor metody czym to jest

    public Book(String author, String title, int pubYear) {
        this.author = author;
        this.title = title;
        this.pubYear = pubYear;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPubYear() {
        return pubYear;
    }

    public void setPubYear(int pubYear) {
        this.pubYear = pubYear;
    }
   public void print() { // metoda pozwalająca drukować 3 informacje
    System.out.println(title + " " + author + " " + pubYear);
}
}
